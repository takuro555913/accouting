﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Account.Utils
{
    public static class AccountDefine
    {

        #region Grid関連
        public enum AccountHeader
        {
            [DisplayText("日付")]
            Day = 0,
            [DisplayText("借方")]
            Debit,
            [DisplayText("")]
            DebitMoney,
            [DisplayText("貸方")]
            Credit,
            [DisplayText("")]
            CreditMoney,
            [DisplayText("備考")]
            Remarks,
        }
        #endregion Grid関連

        #region MenuStrip関連
        public enum MenuEnum : int
        {
            [DisplayText("行追加")]
            AddRow,
            [DisplayText("解除")]
            Release,
            [DisplayText("結合")]
            Merge,
            [DisplayText("閉じる")]
            Close,
            [DisplayText("保存")]
            Save,
        }
        #endregion MenuStrip関連

        #region AccountForm関連
        public const string C_ConfirmSendMail = "変更メールを送信しますか？";
        public const string C_MismatchMoney = "金額が一致していない行があります。";
        public const string C_DirtyRow = "不正な行が存在します。";
        public const string C_Saved = "保存しました。";
        public const string C_ConfirmSave = "保存しますか？";
        public const string C_ErrSaved = "保存に失敗しました。";
        public const string C_ConfirmEnd = "終了しますか？";
        public const string C_ChangedAccountBook = "帳簿は変更されています。";
        public const string C_ErrGetAccountBook = "帳票の取得に失敗しました。";
        public const string C_ErrMearge = "結合行として選択されている行が相応しくありません。";
        public const string C_ErrReleaseMearge = "結合行を選択してください。";

        #endregion AccountForm関連

        #region Common
        public enum UserEnum : int
        {
            [DisplayText("管理者")]
            Admin,
            [DisplayText("一般ユーザ")]
            GeneralUser
        }

        /// <summary>
        /// データベースからのデータ取得グリッドカラム
        /// </summary>
        public enum DbGridColum : int
        {
            [DisplayText("MyIndex")]
            MyIndex,
            [DisplayText("親Index")]
            ParentIndex,
            [DisplayText("日")]
            Day,
            [DisplayText("借方")]
            DebitTitle,
            [DisplayText("借方金額")]
            DebitMoney,
            [DisplayText("貸方")]
            CreditTitle,
            [DisplayText("貸方金額")]
            CreditMoney,
            [DisplayText("備考")]
            Remarks
        }
        #endregion Common

        #region YearMonth
        public enum YearMonth : int
        {
            [DisplayText("年")]
            Year = 0,
            [DisplayText("月")]
            Month,
        }
        #endregion

        #region AccountTitle
        public enum AccountTitle : int
        {
            [DisplayText("現金")]
            Genkin = 0,
            [DisplayText("小口現金")]
            KoguchiGenkin,
            [DisplayText("普通預金")]
            NormalDeposit,
            [DisplayText("当座預金")]
            TozaDeposit,
            [DisplayText("備品")]
            Fixtures,
            [DisplayText("売掛金")]
            Urikakekin,
            [DisplayText("買掛金")]
            Kaikakekin,
            [DisplayText("立替金")]
            Tatekaekin,
            [DisplayText("仮払金")]
            Karibaraikin,
            [DisplayText("建物")]
            Building,
            [DisplayText("車両運搬具")]
            Vehicles,
            [DisplayText("開発費")]
            DevelopmentCost,
            [DisplayText("未払金")]
            AccountsPayable,
            [DisplayText("前受金")]
            Maewatashikin,
            [DisplayText("資本金")]
            CapitalStok
        }
        #endregion AccountTitle

        #region Mail
        public const int C_GmailPort = 587;
        public const string C_SendMail = "送信しました。";
        public const string C_ErrSendMail = "送信に失敗しました。";
        public const string C_ConfirmResend = "再送信しますか？";
        public static string GetSubject() {
            StringBuilder wResult = new StringBuilder();
            wResult.Append(DateTime.Today.ToString("yyyy"));
            wResult.Append("年");
            wResult.Append(DateTime.Today.ToString("MM"));
            wResult.Append("月");
            wResult.Append("の帳票更新のお知らせ");
            return wResult.ToString();
        }
        #endregion Mail

    }

    public static class KeyEventArgsExtention
    {
        public static bool WithControl(this KeyEventArgs e)
        {
            return e.Control && !e.Alt && !e.Shift;

        }

        public static bool WithShift(this KeyEventArgs e)
        {
            return e.Shift && !e.Control && !e.Alt;
        }

        public static bool WithAlt(this KeyEventArgs e)
        {
            return e.Alt && !e.Control && !e.Shift;
        }

        public static bool NounUsually(this KeyEventArgs e)
        {
            return !e.Control && !e.Alt && !e.Shift;
        }

    }
}
