﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Account.Model;
using Account.Utils;
using Account.Mail;
using static Account.Utils.AccountDefine;
using static Account.AccountUtil;
using System.Text;

namespace Account.View
{
    //子画面が来たときの処理を追記する
    class AccountMenuStrip : MenuStrip
    {
        #region メンバ変数
        private UserEnum FUserMode;
        private AccountForm FAccountForm;
        private List<AccountRow> FAccountRowList;
        private string FMailAddress;
        private string FMailPass;
        /// <summary>
        /// 行追加
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem MenuAddRow;
        /// <summary>
        /// 結合
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem MenuMerge;
        /// <summary>
        /// 保存
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem MenuSave;
        /// <summary>
        /// 閉じる
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem MenuClose;
        /// <summary>
        /// 解除
        /// </summary>
        private System.Windows.Forms.ToolStripMenuItem MenuRelease;
        #endregion メンバ変数

        public AccountMenuStrip(UserEnum vUserMode, AccountForm vForm, List<AccountRow> vAccountRowList, string vMailAddress, string vMailPass)
        {
            FAccountForm = vForm;
            FUserMode = vUserMode;
            FMailAddress = vMailAddress;
            FMailPass = vMailPass;
            FAccountRowList = vAccountRowList;
            CommonInitialize();
            switch (vUserMode)
            {
                case UserEnum.Admin:
                    AdminInitialize();
                    break;
                case UserEnum.GeneralUser:
                    GeneralInitialize();
                    break;
            }
        }

        #region 共通
        private void CommonInitialize()
        {
            this.MenuClose = new ToolStripMenuItem();
            this.Items.Add(this.MenuClose);
            this.MenuClose.Tag = AccountDefine.MenuEnum.Close;
            this.MenuClose.Visible = true;
            this.MenuClose.Text = AccountDefine.MenuEnum.Close.GetDisplayText();
            this.MenuClose.Alignment = ToolStripItemAlignment.Right;
        }


        #endregion 共通

        #region 一般ユーザ用
        private void GeneralInitialize()
        {
            this.MenuClose.Click += (_, __) => FAccountForm.Close();

        }
        #endregion 一般ユーザ用


        #region 管理者用
        private void AdminInitialize()
        {
            this.MenuMerge = new ToolStripMenuItem();
            this.MenuRelease = new ToolStripMenuItem();
            this.MenuAddRow = new ToolStripMenuItem();
            this.MenuSave = new ToolStripMenuItem();
            this.Items.AddRange(new ToolStripItem[] {
                        this.MenuClose,
                        this.MenuSave,
                        this.MenuAddRow,
                        this.MenuRelease,
                        this.MenuMerge
                    });
            this.MenuMerge.Text = MenuEnum.Merge.GetDisplayText();
            this.MenuRelease.Text = MenuEnum.Release.GetDisplayText();
            this.MenuAddRow.Text = MenuEnum.AddRow.GetDisplayText();
            this.MenuSave.Text = MenuEnum.Save.GetDisplayText();
            this.MenuMerge.Alignment = ToolStripItemAlignment.Right;
            this.MenuRelease.Alignment = ToolStripItemAlignment.Right;
            this.MenuAddRow.Alignment = ToolStripItemAlignment.Right;
            this.MenuSave.Alignment = ToolStripItemAlignment.Right;

            this.MenuAddRow.Click += (_, __) => AddRow();
            this.MenuMerge.Click += (_, __) => MergeRow();
            this.MenuSave.Click += (_, __) => SaveAccountRows();
            this.MenuRelease.Click += (_, __) => ReleaseMergeRows();
        }

        /// <summary>
        /// 行追加
        /// </summary>
        private void AddRow()
        {
            FAccountRowList.Add(new AccountRow(FAccountRowList.GetNewIndex()));
            FAccountForm.GridRefresh();
        }

        /// <summary>
        /// 行結合
        /// </summary>
        private void MergeRow()
        {
            //グリッドの選択行をリスト化
            List<int> wGridSelectedIndex = new List<int>();
            foreach (DataGridViewRow wDataGridViewRow in FAccountForm.GetSelectedRows())
                wGridSelectedIndex.Add(wDataGridViewRow.Index);

            //連続した複数行かチェック処理
            if (!IsCheckMerge(wGridSelectedIndex))
            {
                AccountUtil.ShowErrorDialog(AccountDefine.C_ErrMearge);
                return;
            }

            //結合処理開始
            //結合の結果親となる行を抽出
            var MergeIndex = this.FAccountRowList.Where(x => x.MyIndex == wGridSelectedIndex.Min());

            //親となるインデックスを子のインデックスへセットする
            this.FAccountRowList.AsEnumerable()
                .Where(x => wGridSelectedIndex.IndexOf(x.MyIndex) != -1)
                .Select(y => y.ParentIndex = MergeIndex.First().MyIndex);

            //親となるインデックスを子のインデックスへセットする
            foreach (var wAccountRow in this.FAccountRowList)
                if (wGridSelectedIndex.IndexOf(wAccountRow.MyIndex) != -1)
                {
                    wAccountRow.ParentIndex = MergeIndex.First().MyIndex;
                    //日付と備考のデータを空にする
                    if (wAccountRow.MyIndex != MergeIndex.First().MyIndex)
                    {
                        wAccountRow.Day = null;
                        wAccountRow.Remarks = string.Empty;
                    }
                }
            FAccountForm.GridRefresh();
        }

        /// <summary>
        /// 結合行解除
        /// </summary>
        private void ReleaseMergeRows()
        {
            //グリッドの選択行をリスト化
            List<int> wGridSelectedIndex = new List<int>();
            foreach (DataGridViewRow wDataGridViewRow in FAccountForm.GetSelectedRows())
                wGridSelectedIndex.Add(wDataGridViewRow.Index);

            //結合行の一部のみが選択されているか確認
            var wMergeRowIndex = this.FAccountRowList.FindAll(x => x.ParentIndex == this.FAccountRowList[wGridSelectedIndex.Min()].MyIndex);
            if (wMergeRowIndex.Count() != wGridSelectedIndex.Count())
            {
                AccountUtil.ShowErrorDialog(AccountDefine.C_ErrReleaseMearge);
                return;
            }

            foreach (var wIndex in wGridSelectedIndex)
            {
                //結合行の選択か確認
                if (this.FAccountRowList[wIndex].ParentIndex == -1)
                {
                    AccountUtil.ShowErrorDialog(AccountDefine.C_ErrReleaseMearge);
                    return;
                }
            }

            //行の結合を解除し、日付にデータをセットする
            foreach(var wIndex in wGridSelectedIndex)
            {
                this.FAccountRowList[wIndex].ParentIndex = -1;
                this.FAccountRowList[wIndex].Day = this.FAccountRowList[wGridSelectedIndex.Min()].Day;
            }

            FAccountForm.GridRefresh();
        }


        /// <summary>
        /// グリッドデータ保存処理
        /// </summary>
        private void SaveAccountRows()
        {
            var wMergeRowList = FAccountRowList.Where(x => x.ParentIndex != -1).GroupBy(x => x.ParentIndex);
            var wSingleRowList = FAccountRowList.Where(x => x.ParentIndex == -1);
            // 結合をしていない行の金額差異チェック
            foreach (var wAccountRow in wSingleRowList) {
                if (wAccountRow.Credit.Money == wAccountRow.Debit.Money) continue;
                ShowErrorDialog(C_MismatchMoney);
                return;
            }
            // 結合をしている行の金額差異チェック
            foreach (var wAccountRowList in wMergeRowList)
            {
                var wCreditMoney = wAccountRowList.Sum(x => x.Credit.Money);
                var wDebitMoney = wAccountRowList.Sum(x => x.Credit.Money);
                if (wCreditMoney == wDebitMoney) continue;
                ShowErrorDialog(C_MismatchMoney);
                return;
            }
            // 一番最後尾の行でない空の行があるか？
            var wAccountRecord = FAccountRowList.Find(x => x.GetIsEmpty());
            if (wAccountRecord != null && wAccountRecord.MyIndex == FAccountRowList.Count - 1) {
                ShowErrorDialog(C_DirtyRow);
                return;
            }

            
            var wYearMonth = this.FAccountForm.GetMonthData;
            DbConnect wDb = new DbConnect();

            //現在表示中のグリッドデータの年月日取得
            var wCalendarIDReader = wDb.ExecSqlReader(wDb.SqlSelectCalendarID(wYearMonth.Year, wYearMonth.Month));
            int wCalendarID = 0;

            while (wCalendarIDReader.Read())
                wCalendarID = (int)wCalendarIDReader.GetValue(0);

            wDb.Connection.Close();

            //トランザクション開始
            wDb.Connection.Open();
            MySqlTransaction wTransaction = wDb.Connection.BeginTransaction(IsolationLevel.ReadCommitted);

            try
            {
                wDb.CreateMySqlCommand(wDb.SqlDelBalance(wYearMonth.Year, wYearMonth.Month)).ExecuteNonQuery();
                foreach (var wAccountRow in this.FAccountRowList)
                {
                    //結合したデータの日付取得処理
                    AccountRow wDay = wAccountRow;
                    if (this.FAccountRowList.Find(x => x.MyIndex == wAccountRow.ParentIndex) != null)
                        wDay = this.FAccountRowList.Find(x => x.MyIndex == wAccountRow.ParentIndex);

                    //グリッド行文INSERT文実行
                    wDb.CreateMySqlCommand(wDb.SqlInsertBalance(
                        wCalendarID,
                        wAccountRow.MyIndex,
                        wAccountRow.ParentIndex,
                        wDay.Day,
                        wAccountRow.Debit.AccountTitle,
                        wAccountRow.Debit.Money,
                        wAccountRow.Credit.AccountTitle,
                        wAccountRow.Credit.Money,
                        wAccountRow.Remarks)).ExecuteNonQuery();
                }

                //コミット
                wTransaction.Commit();

                //保存完了ダイアログ
                if (ShowInformationDialog(C_Saved + Environment.NewLine + C_ConfirmSendMail)) {
                    SendMail wMail = new SendMail(this.FMailAddress, this.FMailPass);
                    wMail.SetPort = C_GmailPort;
                    wMail.SetHost = "smtp.gmail.com";
                    wMail.SetSubject = GetSubject();
                    var wAccountRowList = FAccountForm.AccountRowList.GetAddAccountRow(FAccountRowList);
                    StringBuilder wMailBody = new StringBuilder();
                    foreach (var wAccountRow in wAccountRowList) {
                        wMailBody.Append("【追加】");
                        wMailBody.Append(Environment.NewLine);
                        wMailBody.Append("　" +(wAccountRow.MyIndex - 1).ToString() + "行目");
                        wMailBody.Append(Environment.NewLine);
                        wMailBody.Append("　" + "日付：" + wAccountRow.Day);
                        wMailBody.Append(Environment.NewLine);
                        wMailBody.Append("　" + wAccountRow.Credit.AccountTitle + "：" + wAccountRow.Credit.Money.ToString());
                        wMailBody.Append(" / ");
                        wMailBody.Append(wAccountRow.Debit.AccountTitle + "：" + wAccountRow.Debit.Money.ToString());
                        wMailBody.Append(Environment.NewLine);
                        wMailBody.Append("　" + "備考：" + wAccountRow.Remarks);
                        wMailBody.Append(Environment.NewLine);
                    }
                    wMail.SetBody = wMailBody.ToString();
                    // ToAddressを設定すること
                    wMail.SetFromAddress = this.FMailAddress;
                    wMail.SetToAddress = GetSendMailAddress(this.FMailAddress);
                    wMail.Send();
                    

                }
            }
            catch (System.Exception e)
            {
                //ロールバック処理
                wTransaction.Rollback();
                AccountUtil.ShowErrorDialog(AccountDefine.C_ErrSaved);
                //throw;
            }
            finally
            {
                wDb.Connection.Close();
            }

        }

        /// <summary>
        /// 送信先メールアドレスリスト取得
        /// </summary>
        /// <param name="vMyAddress"></param>
        /// <returns></returns>
        private List<string> GetSendMailAddress(string vMyAddress)
        {
            DbConnect wDB = new DbConnect();
            MySqlDataReader wMailAddresss = wDB.ExecSqlReader(wDB.SqlSelectSendMailList(vMyAddress));

            List<string> wMailList = new List<string>();
            while (wMailAddresss.Read())
                wMailList.Add(wMailAddresss.GetValue(0).ToString());
            wDB.ConnectionClose();

            return wMailList;
        }

        /// <summary>
        /// 結合が可能かチェックする
        /// </summary>
        /// <param name="vGridSelectedIndex"></param>
        /// <returns></returns>
        private bool IsCheckMerge(List<int> vGridSelectedIndex)
        {
            //選択行が複数ではない場合FALSE
            if (vGridSelectedIndex.Count <= 1)
                return false;

            var wOrderByList = vGridSelectedIndex.OrderBy(x => x);

            int wIndex = wOrderByList.First();
            foreach (var wNextIndex in wOrderByList)
            {
                //選択行数が連続したものの場合、またはすでに結合行の場合FALSE
                if ((wNextIndex - wIndex != 1 && wIndex != wNextIndex) || this.FAccountRowList[wNextIndex].ParentIndex != -1)
                    return false;

                wIndex = wNextIndex;
            }

            return true;
        }
        #endregion 管理者用
    }
}
