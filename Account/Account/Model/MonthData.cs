﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Model
{
    [System.Runtime.Serialization.DataContract]
   public class MonthData
    {
        [System.Runtime.Serialization.DataMember()]
        public List<AccountRow> AccountRowList = new List<AccountRow>();

        [System.Runtime.Serialization.DataMember()]
        public string Year;

        [System.Runtime.Serialization.DataMember()]
        public string Month;
    }
}
