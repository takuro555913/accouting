﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Model
{
    [System.Runtime.Serialization.DataContract]
   public class AccountCell
    {
        [System.Runtime.Serialization.DataMember()]
        public int Money;

        [System.Runtime.Serialization.DataMember()]
        public string AccountTitle;
    }
}
