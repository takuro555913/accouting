﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Account.Model;
using MySql.Data.MySqlClient;
using static Account.Utils.AccountDefine;

namespace Account
{
    public partial class LoginForm : Form
    {

        private UserEnum FUserMode;
        private string FMailAddress;
        private string FMailPass;

        public UserEnum GetUserMode
        {
            get { return this.FUserMode; }
        }
        public string GetFMailAddress
        {
            get { return this.FMailAddress; }
        }
        public string GetFMailPass
        {
            get { return this.FMailPass; }
        }

        public LoginForm()
        {
            InitializeComponent();
            this.FormClosing += (object sender, System.Windows.Forms.FormClosingEventArgs e) => { Properties.Settings.Default.Save(); };
            this.btn_OK.Click += (_, __) =>
            {
                if (!IsMatchPassword(this.LoginText.Text))
                    this.DialogResult = DialogResult.None;
            };
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData) {
                case Keys.Enter:
                    DialogResult = DialogResult.OK;
                    break;
            }
        }

        //パスワード一致確認メソッド
        public bool IsMatchPassword(string vPass)
        {
            DbConnect wDb = new DbConnect();
            var wUserInfo = wDb.ExecSqlReader(wDb.SqlSelectUserInfo(vPass));
            if (wUserInfo.HasRows)
            {
                //ログインモードをセット
                while (wUserInfo.Read())
                {
                    //GetValueの引数:(0→ユーザーモード, 1→メールアドレス, 2→メールパスワード)
                    this.FUserMode = (UserEnum)Enum.Parse(typeof(UserEnum), wUserInfo.GetValue(0).ToString(), false); 
                    this.FMailAddress = wUserInfo.GetValue(1).ToString();
                    this.FMailPass = wUserInfo.GetValue(2).ToString();
                }


                return true;
            }

            this.NotPassMatchLabel.Visible = true;
            return false;
        }

        //ログイン時データ読み込み
        public List<AccountRow> ReadDBGridData(string vYear, string vMonth)
        {
            DbConnect wDB = new DbConnect();
            MySqlDataReader wGridData = wDB.ExecSqlReader(wDB.SqlSelectGridData(vYear, vMonth));

            //取得したグリッドデータをディクショナリに変換しリターン
            var wDicGridData = AccountUtil.ChangeMysqlGridDataToAccountRowList(wGridData);
            wDB.ConnectionClose();

            return wDicGridData;
        }


        public MySqlDataReader ReadDBYearMonth(string vOneYearAgo, string vMonth)
        {
            DbConnect wDb = new DbConnect();
            //直近1年分の年月取得
            MySqlDataReader wYearMonth = wDb.ExecSqlReader(wDb.SqlSelectOneYear(vOneYearAgo, vMonth));

            return wYearMonth;
        }
    }
}
