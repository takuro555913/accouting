﻿using Account.View;
using Account.Grid;
using System.Windows.Forms;

namespace Account
{
    partial class AccountForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonDisplay = new System.Windows.Forms.Button();
            this.YearComboBox = new System.Windows.Forms.ComboBox();
            this.MonthComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // ButtonDisplay
            // 
            this.ButtonDisplay.Location = new System.Drawing.Point(208, 37);
            this.ButtonDisplay.Name = "ButtonDisplay";
            this.ButtonDisplay.Size = new System.Drawing.Size(99, 23);
            this.ButtonDisplay.TabIndex = 1;
            this.ButtonDisplay.Text = "表示";
            this.ButtonDisplay.UseVisualStyleBackColor = true;
            this.ButtonDisplay.Click += new System.EventHandler(this.ButtonDisplay_Click);
            // 
            // YearComboBox
            // 
            this.YearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.YearComboBox.FormattingEnabled = true;
            this.YearComboBox.Location = new System.Drawing.Point(24, 40);
            this.YearComboBox.Name = "YearComboBox";
            this.YearComboBox.Size = new System.Drawing.Size(80, 20);
            this.YearComboBox.TabIndex = 2;
            // 
            // MonthComboBox
            // 
            this.MonthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MonthComboBox.FormattingEnabled = true;
            this.MonthComboBox.Location = new System.Drawing.Point(116, 40);
            this.MonthComboBox.Name = "MonthComboBox";
            this.MonthComboBox.Size = new System.Drawing.Size(80, 20);
            this.MonthComboBox.TabIndex = 3;
            // 
            // AccountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 500);
            this.Controls.Add(this.MonthComboBox);
            this.Controls.Add(this.YearComboBox);
            this.Controls.Add(this.ButtonDisplay);
            this.DataBindings.Add(new System.Windows.Forms.Binding("WindowState", global::Account.Properties.Settings.Default, "MainFormState", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::Account.Properties.Settings.Default, "FormLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.KeyPreview = true;
            this.Location = global::Account.Properties.Settings.Default.FormLocation;
            this.Name = "AccountForm";
            this.ShowIcon = false;
            this.Text = "AccountForm";
            this.WindowState = global::Account.Properties.Settings.Default.MainFormState;
            this.ResumeLayout(false);

        }

        #endregion
        private Button ButtonDisplay;
        private AccountMenuStrip AccountMenuStrip;
        private AccountGrid AccountGrid;
        private ComboBox YearComboBox;
        private ComboBox MonthComboBox;
    }
}

