﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Account.Grid;
using System.IO;
using Newtonsoft.Json;
using Account.Model;
using Account.View;
using static Account.Utils.AccountDefine;
using static Account.Utils.TestData;
using MySql.Data.MySqlClient;

namespace Account
{
    public partial class AccountForm : Form
    {
        private UserEnum FUserMode;
        private MonthData FMonthData = new MonthData();
        private MySqlDataReader FYearMonth;
        private string FMailAddress;
        private string FMailPass;

        public MonthData GetMonthData
        {
            get { return this.FMonthData; }
        }

        // 編集不可のDBAccountRowList(保存の際、比較する時に使用します)
        public List<AccountRow> AccountRowList { get; }

        #region コンストラクタ
        public AccountForm(UserEnum vUserMode, MySqlDataReader vYearMonth, List<AccountRow> vGridData, string vMailAddress, string vMailPass)
        {
            FMonthData.AccountRowList = vGridData;
            this.AccountRowList = new List<AccountRow>(FMonthData.AccountRowList);
            FYearMonth = vYearMonth;
            FUserMode = vUserMode;
            FMailAddress = vMailAddress;
            FMailPass = vMailPass;
            InitializeComponent();
            CommonInitialize();
            Refresh();


            switch (vUserMode) {
                case UserEnum.Admin:
                    break;
                case UserEnum.GeneralUser:
                    break;
            }
        }

        private void CommonInitialize()
        {
            GridInitialize();
            this.SetCombYearMonth(this.FYearMonth);
            this.FMonthData.Year = this.YearComboBox.Text;
            this.FMonthData.Month = this.MonthComboBox.Text;

            this.AccountMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.AccountMenuStrip.Name = "AccountMenuStrip";
            this.AccountMenuStrip.Size = new System.Drawing.Size(799, 24);
            this.AccountMenuStrip.TabIndex = 4;
            this.AccountMenuStrip.Text = "AccountMenuStrip";
        }

        private void GridInitialize()
        {
            this.AccountGrid = new AccountGrid(this.FUserMode, this.FMonthData.AccountRowList);
            this.AccountMenuStrip = new AccountMenuStrip(this.FUserMode, this, this.FMonthData.AccountRowList, this.FMailAddress, this.FMailPass);

            this.AccountGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AccountGrid.Location = new System.Drawing.Point(50, 106);
            this.AccountGrid.Size = new System.Drawing.Size(700, 352);
            this.AccountGrid.TabIndex = 4;
            this.AccountGrid.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
            this.AccountGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.Grid_CellPainting);
            this.AccountGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellValueChanged);

            this.Controls.Add(this.AccountGrid);
            this.Controls.Add(this.AccountMenuStrip);
        }


        private void SetCombYearMonth(MySqlDataReader vYearMonth)
        {
            while (vYearMonth.Read())
            {
                if (this.YearComboBox.Items.IndexOf(vYearMonth[(int)YearMonth.Year].ToString()) == -1)
                    this.YearComboBox.Items.Add(vYearMonth[0].ToString());

                if (this.MonthComboBox.Items.IndexOf(vYearMonth[(int)YearMonth.Month].ToString()) == -1)
                    this.MonthComboBox.Items.Add(vYearMonth[1].ToString());
            }

            //現在の年月をコンボボックスの初期値としてセット
            DateTime wDt = DateTime.Now;
            this.YearComboBox.SelectedIndex = this.YearComboBox.FindString(wDt.Year.ToString());
            this.MonthComboBox.SelectedIndex = this.MonthComboBox.FindString(wDt.Month.ToString());
        }
        #endregion

        /// <summary>
        /// 表示ボタン押下時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                //年月からSQL文作成
                DbConnect wDB = new DbConnect();
                MySqlDataReader wGridData = wDB.ExecSqlReader(wDB.SqlSelectGridData(this.YearComboBox.Text, this.MonthComboBox.Text));

                //取得したグリッドデータをディクショナリに変換しリターン
                var wDicGridData = AccountUtil.ChangeMysqlGridDataToAccountRowList(wGridData);
                wDB.ConnectionClose();

                this.FMonthData.AccountRowList = wDicGridData;
                this.FMonthData.Year = this.YearComboBox.Text;
                this.FMonthData.Month = this.MonthComboBox.Text;

                //グリッド再描画
                GridRefresh();
            }
            catch (System.Exception)
            {
                AccountUtil.ShowErrorDialog(C_ErrGetAccountBook);
            }
        }

        private void Grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var wGrid = ((AccountGrid)sender);
            var wText = wGrid.EditingControl.Text;
            switch ((AccountHeader)e.ColumnIndex)
            {
                case AccountHeader.Day:
                    this.FMonthData.AccountRowList[e.RowIndex].Day = wText;
                    break;
                case AccountHeader.Debit:
                    this.FMonthData.AccountRowList[e.RowIndex].Debit.AccountTitle = wText;
                    break;
                case AccountHeader.DebitMoney:
                        this.FMonthData.AccountRowList[e.RowIndex].Debit.Money = int.Parse(wText);
                    break;
                case AccountHeader.Credit:
                    this.FMonthData.AccountRowList[e.RowIndex].Credit.AccountTitle = wText;
                    break;
                case AccountHeader.CreditMoney:
                    this.FMonthData.AccountRowList[e.RowIndex].Credit.Money = int.Parse(wText);
                    break;
                case AccountHeader.Remarks:
                    this.FMonthData.AccountRowList[e.RowIndex].Remarks = wText;
                    break;
            }
        }

        private void Grid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            List<int> wMaxMergeIndex = new List<int>();
            List<int> wMinMergeIndex = new List<int>();
            List<int> wOtherMergeIndex = new List<int>();

            //結合行のみ抽出
            var wMargeRows = this.FMonthData.AccountRowList
                .GroupBy(i => i.ParentIndex)
                .Where(g => g.Count() > 1 && g.Any(x => x.ParentIndex != -1));

            foreach (var wMargeRow in wMargeRows)
            {
                List<int> wMergeIndexList = new List<int>();
                foreach (var wMargeIndex in wMargeRow)
                    wMergeIndexList.Add(wMargeIndex.MyIndex);

                wMaxMergeIndex.Add(wMergeIndexList.Max());
                wMinMergeIndex.Add(wMergeIndexList.Min());

                wMergeIndexList.Remove(wMergeIndexList.Max());
                wMergeIndexList.Remove(wMergeIndexList.Min());
                foreach (var wIndex in wMergeIndexList) wOtherMergeIndex.Add(wIndex);
            }

            switch (e.ColumnIndex)
            {
                case (int)AccountHeader.Day:
                case (int)AccountHeader.Remarks:
                    if (wMaxMergeIndex.IndexOf(e.RowIndex) != -1)
                        e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;

                    else if(wMinMergeIndex.IndexOf(e.RowIndex) != -1)
                        e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;

                    else if(wOtherMergeIndex.IndexOf(e.RowIndex) != -1)
                    {
                        e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
                        e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// AccountGridの再描画処理
        /// </summary>
        public void GridRefresh()
        {
            this.Controls.Remove(this.AccountGrid);
            this.Controls.Remove(this.AccountMenuStrip);
            GridInitialize();
            Refresh();
        }

        /// <summary>
        /// AccountGridの選択行取得メソッド
        /// </summary>
        /// <returns></returns>
        public DataGridViewSelectedRowCollection GetSelectedRows()
        {
            return this.AccountGrid.SelectedRows;
        }
    }
}
