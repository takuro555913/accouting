﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Account
{
    //残り行うこと
    //・接続情報をXMLファイルにする

    class DbConnect
    {
        //接続用オブジェクト
        public MySqlConnection Connection;
        //変更前グリッドデータ
        MySqlDataReader BeforeGridData;

        public MySqlDataReader UpdateBeforeGridData
        {
            set { BeforeGridData = value; }
        }

        //コンストラクタ
        public DbConnect()
        {
            //接続情報(テスト用)
            string wServer = "VOSTRO0316";
            string wUser = "takuro";           // MySQLユーザ名
            string wPass = "1015";           // MySQLパスワード
            string wDatabase = "account_system";		// 接続するデータベース名
            string wCharset = "utf8";

            string connectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};SslMode=none;Charset='{4}'", wServer, wDatabase, wUser, wPass, wCharset);
            // MySQLへの接続
            try
            {
                this.Connection = new MySqlConnection(connectionString);
            }
            catch (MySqlException me)
            {
                //ToDo ダイアログに変更
                Console.WriteLine("ERROR: " + me.Message);
            }
        }

        //MysqlConnectionオブジェクトを閉じる
        public void ConnectionClose()
        {
            this.Connection.Close();
        }

        //ToDO SQL文生成メソッド作成
        public string SqlSelectGridData(string vYear, string vMonth)
        {
            string wSql =
                string.Format(
                "SELECT " +
                    "rowindex, " +
                    "parentindex, " +
                    "day, karikata, " +
                    "karikatakingaku, " +
                    "kashikata, " +
                    "kashikatakingaku, " +
                    "biko " +
                "FROM calendar " +
                "INNER JOIN balance " +
                "ON ( " +
                    "calendar.calendarID = balance.calendarID" +
                ") " +
                "WHERE calendar.year = {0} AND calendar.month = {1} " +
                "ORDER BY rowindex"
                , vYear, vMonth);
            return wSql;
        }

        public string SqlSelectUserInfo(string vPass)
        {
            string wSql =
                string.Format(
                "SELECT " +
                    " modename," +
                    " mailaddress," +
                    " mailpass " +
                "FROM user_ " +
                "INNER JOIN mode_ " +
                "ON ( " +
                    "user_.loginmodeID = mode_.loginmodeID " +
                ") " +
                "WHERE user_.pass = '{0}'",
                vPass);
            return wSql;
        }

        public string SqlSelectSendMailList(string vMyAddress)
        {
            string wSql =
                string.Format(
                "SELECT " +
                    " mailaddress " +
                "FROM user_ " +
                "WHERE mailaddress != '{0}'",
                vMyAddress);
            return wSql;
        }

        public string SqlSelectMaxCalendarID()
        {
            return "SELECT MAX(calendarID) FROM calendar";
        }

        public string SqlSelectCountCalendarRow(string vYear, string vMonth)
        {
            string wSql =
                string.Format(
                "SELECT " +
                    "count(calendarID) " +
                "FROM calendar " +
                "WHERE calendar.year = {0} AND calendar.month = {1} "
                , vYear.ToString(), vMonth.ToString());
            return wSql;
        }

        /// <summary>
        /// 直近1年の年月を取得
        /// </summary>
        /// <param name="vYear">1年前の年</param>
        /// <param name="vMonth">現在の月</param>
        /// <returns></returns>
        public string SqlSelectOneYear(string vYear, string vMonth)
        {
            string wSql =
                string.Format(
                "SELECT " +
                    "year, " +
                    "month " +
                "FROM calendar " +
                "WHERE calendar.year >= {0} AND calendar.month <= {1} " +
                "ORDER BY year, month"
                , vYear, vMonth);
            return wSql;
        }

        /// <summary>
        /// CalendarID取得用SELECT文
        /// </summary>
        /// <param name="vYear"></param>
        /// <param name="vMonth"></param>
        /// <returns></returns>
        public string SqlSelectCalendarID(string vYear, string vMonth)
        {
            string wSql =
                string.Format(
                "SELECT " +
                    "calendarID " +
                "FROM calendar " +
                "WHERE calendar.year = {0} AND calendar.month = {1} "
                ,vYear, vMonth);
            return wSql;
        }

        public string SqlUpdateGridData(string vYear, string vMonth)
        {
            string wSql =
                string.Format(
                "UPDATE " +
                    "balance " +
                "SET " +
                    "parentindex = 1, " + //←テストのため固定値
                    "day = 15 " + //←テストのため固定値
                "WHERE calendarID = (SELECT calendarID FROM calendar WHERE calendar.year = {0} AND calendar.month = {1})"
                , vYear.ToString(), vMonth.ToString());
            return wSql;
        }

        public string SqlInsertCalendar(int vCalendarID, int vYear, int vMonth)
        {
            string wSql =
                string.Format(
                "INSERT INTO " +
                    "calendar " +
                "VALUES " +
                    "({0}, {1}, {2}) "
                , vCalendarID.ToString(), vYear.ToString(), vMonth.ToString());
            return wSql;
        }

        /// <summary>
        /// グリッドデータ保存用INSERT文
        /// </summary>
        /// <param name="vCalendarID"></param>
        /// <param name="vRowIndex"></param>
        /// <param name="vParentIndex"></param>
        /// <param name="vDay"></param>
        /// <param name="vKarikata"></param>
        /// <param name="vKarikatakingaku"></param>
        /// <param name="vKashikata"></param>
        /// <param name="vKashikatakingaku"></param>
        /// <param name="vBiko"></param>
        /// <returns></returns>
        public string SqlInsertBalance(int vCalendarID, int vRowIndex, int vParentIndex, string vDay, string vKarikata, int vKarikatakingaku, string vKashikata, int vKashikatakingaku, string vBiko)
        {

            string wSql =
                string.Format(
                "INSERT INTO " +
                    "balance " +
                    "(calendarID, rowindex, parentindex, day, karikata, karikatakingaku, kashikata, kashikatakingaku, biko) " +
                "VALUES " +
                    "({0}, {1}, {2}, {3}, '{4}', {5}, '{6}', {7}, '{8}') "
                , vCalendarID.ToString()
                , vRowIndex.ToString()
                , vParentIndex.ToString()
                , vDay.ToString()
                , vKarikata
                , vKarikatakingaku.ToString()
                , vKashikata
                , vKashikatakingaku.ToString()
                , vBiko
                );
            return wSql;
        }

        public string SqlDelBalance(string vYear, string vMonth)
        {
            string wSql =
                string.Format(
                "DELETE balance " +
                    "FROM balance " +
                    "INNER JOIN calendar " +
                        "ON balance.calendarID = calendar.calendarID " +
                "WHERE calendar.year = {0} AND calendar.month = {1}"
                , vYear, vMonth);
            return wSql;
        }

        //ToDo データ取得用SQL文実行メソッド
        public MySqlDataReader ExecSqlReader(string vSql)
        {
            this.Connection.Open();
            MySqlDataReader wResult = CreateMySqlCommand(vSql).ExecuteReader();
            //this.Connection.Close();

            return wResult;
        }

        //ToDo データ変更用SQL文実行メソッド
        public void ExecSqlNonQuery(string vSql)
        {
            this.Connection.Open();
            int wTargetRow = CreateMySqlCommand(vSql).ExecuteNonQuery();
            //this.Connection.Close();
        }

        //ToDo 集計関数用SQL文実行メソッド
        public object ExecSqlScalar(string vSql)
        {
            this.Connection.Open();
            var wResult = CreateMySqlCommand(vSql).ExecuteScalar();
            //this.Connection.Close();

            return wResult;
        }

        public MySqlCommand CreateMySqlCommand(string vSql)
        {
            MySqlCommand wCommand = new MySqlCommand(vSql, this.Connection);
            return wCommand;
        }
    }
}
