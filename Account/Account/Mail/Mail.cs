﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Windows.Forms;

namespace Account.Mail
{
    public class SendMail
    {
        #region メンバ変数
        private MailMessage FMsg = new MailMessage();
        private SmtpClient FSmtpClient = new SmtpClient();
        #endregion メンバ変数

        #region プロパティ
        /// <summary>
        /// 送信元アドレスを設定します
        /// </summary>
        public string SetFromAddress
        {
            set { FMsg.From = new MailAddress(value); }
        }
        /// <summary>
        /// 宛先を設定します
        /// </summary>
        public List<string> SetToAddress
        {
            set
            {
                value.ForEach(x => FMsg.To.Add(new MailAddress(x)));
            }
        }
        /// <summary>
        /// 本文を設定します
        /// </summary>
        public string SetBody
        {
            set { FMsg.Body = value; }
        }
        /// <summary>
        /// 題名を設定します
        /// </summary>
        public string SetSubject
        {
            set { FMsg.Subject = value; }
        }
        /// <summary>
        /// ポートを設定します
        /// </summary>
        public int SetPort
        {
            set {
                if (value < 0 || value > 65535) throw new Exception("ポートは0から65535までを指定してください");
                FSmtpClient.Port = value;
            }
        }
        public string SetHost
        {
            set { FSmtpClient.Host = value; }
        }
        #endregion プロパティ

        #region コンストラクタ
        public SendMail(string vAccount, string vPass)
        {
            FSmtpClient.Credentials = new NetworkCredential(vAccount, vPass);
            FSmtpClient.Timeout = 5000;
            FSmtpClient.EnableSsl = true;
        }
        #endregion コンストラクタ

        #region メソッド
        /// <summary>
        /// メールを送信します
        /// </summary>
        /// <returns>送信に失敗したらfalseを返します</returns>
        public bool Send()
        {
            try
            {
                FSmtpClient.Send(FMsg);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }
        #endregion メソッド

    }
}
