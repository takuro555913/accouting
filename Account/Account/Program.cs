﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Account.Utils;
using MySql.Data.MySqlClient;
using Account.Model;

namespace Account
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AccountDefine.UserEnum wUserMode;
            string wMailAddress;
            string wMailPass;
            MySqlDataReader wYearMonth;
            List<AccountRow> wGridData;

            using (LoginForm LoginForm = new LoginForm())
            {
                LoginForm.ShowDialog();
                if (LoginForm.DialogResult == DialogResult.Cancel)
                    return;

                //今日の日付を取得
                DateTime wDt = DateTime.Now;
                string wOneYearAgo = wDt.AddYears(-1).Year.ToString();

                wUserMode = LoginForm.GetUserMode;
                wMailAddress = LoginForm.GetFMailAddress;
                wMailPass = LoginForm.GetFMailPass;
                wYearMonth = LoginForm.ReadDBYearMonth(wOneYearAgo, wDt.Month.ToString());
                wGridData = LoginForm.ReadDBGridData(wDt.Year.ToString(), wDt.Month.ToString());
            }

            Application.Run(new AccountForm(wUserMode, wYearMonth, wGridData, wMailAddress, wMailPass));
        }
    }
}
