﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Record
{

    class BSRecord : DataRecord
    {
        public Tuple<string, int?> Karikata;
        public Tuple<string, int?> Kashikata;



        public BSRecord(Tuple<string, int?> vKarikata, Tuple<string, int?> vKashikata, string vBiko = null)
        {
            this.Karikata = vKarikata;
            this.Kashikata = vKashikata;
            this.ID = CreateID;
            this.Day = DateTime.Now.ToString("dd");
            this.Biko = vBiko;
        }
    }

}
