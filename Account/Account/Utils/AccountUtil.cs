﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Account.Model;
using System.Windows.Forms;
using static Account.Utils.AccountDefine;
using Account.Utils;
using MySql.Data.MySqlClient;

namespace Account
{
    public static class AccountUtil
    {
        #region 拡張メソッド
        /// <summary>
        /// Enumのディスプレイテキストを取得する
        /// </summary>
        /// <param name="vEnumValue"></param>
        /// <returns></returns>
        public static string GetDisplayText(this Enum vEnumValue)
        {
            var wFieldInfo = vEnumValue.GetType().GetField(vEnumValue.ToString());
            if (wFieldInfo == null) return string.Empty;

            var wDisplayAttribute = wFieldInfo.GetCustomAttributes(typeof(DisplayTextAttribute), false).Cast<DisplayTextAttribute>(); if (!wDisplayAttribute.Any()) return string.Empty;
            return wDisplayAttribute.First().DisplayText;
        }

        /// <summary>
        /// List<AccountRow>内に存在しないインデックスを取得する
        /// </summary>
        /// <param name="vAccountRowList"></param>
        /// <returns></returns>
        public static int GetNewIndex(this List<AccountRow> vAccountRowList)
        {
            int wResultIndex = 0;
            while (vAccountRowList.Any(x => x.MyIndex == wResultIndex))
            {
                wResultIndex++;
            }
            return wResultIndex;
        }

        /// <summary>
        /// 新たに追加されたAccountRowを取得します
        /// </summary>
        /// <param name="vthis"></param>
        /// <param name="vCompareList"></param>
        /// <returns></returns>
        public static List<AccountRow> GetAddAccountRow(this List<AccountRow> vthis, List<AccountRow> vCompareList)
        {
            List<AccountRow> wResult = new List<AccountRow>();
            for (int i = vthis.Count; i < vCompareList.Count; i++) wResult.Add(vCompareList[i]);
            return wResult;
        }

        #endregion 拡張メソッド


        /// <summary>
        /// 注意ダイアログを表示する
        /// </summary>
        /// <param name="vMessage">確認メッセージ</param>
        /// <returns></returns>
        public static DialogResult ShowExclamationDialog(string vMessage)
        {
            return MessageBox.Show(vMessage, "注意", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        /// エラーダイアログを表示する
        /// </summary>
        /// <param name="vMessage">確認メッセージ</param>
        /// <returns></returns>
        public static DialogResult ShowErrorDialog(string vMessage)
        {
            return MessageBox.Show(vMessage, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        /// <summary>
        /// 確認ダイアログを表示する
        /// </summary>
        /// <param name="vMessage">確認メッセージ</param>
        /// <returns></returns>
        public static bool ShowInformationDialog(string vMessage)
        {
            if (MessageBox.Show(vMessage, "確認", MessageBoxButtons.YesNo) == DialogResult.Yes) return true;
            return false;
        }

        /// MySQLから取得したグリッドデータをAccountRowListに変換するメソッド
        /// </summary>
        /// <param name="vMysqlGridData">DBから取得したデータ</param>
        public static List<AccountRow> ChangeMysqlGridDataToAccountRowList(MySqlDataReader vMysqlGridData)
        {
            List<AccountRow> AccountRowList = new List<AccountRow>();

            while (vMysqlGridData.Read())
            {
                //結合行の場合日付はnullへ変換
                string wDay = vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.Day).ToString();
                if ((int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.ParentIndex) != -1 &&
                    (int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.MyIndex) != (int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.ParentIndex))
                    wDay = string.Empty;

                AccountRow wAccountRow
                    = new AccountRow(
                        (int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.MyIndex),
                        (int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.ParentIndex),
                        wDay,
                        vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.DebitTitle).ToString(),
                        (int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.DebitMoney),
                        vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.CreditTitle).ToString(),
                        (int)vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.CreditMoney),
                        vMysqlGridData.GetValue((int)AccountDefine.DbGridColum.Remarks).ToString()
                    );

                AccountRowList.Add(wAccountRow);
            }

            return AccountRowList;
        }
    }
}
