﻿using System;

namespace Account.Utils
{
    internal class DisplayTextAttribute : Attribute
    {

        public string DisplayText { get; private set; }

        public DisplayTextAttribute(string vDisplayText) {
            this.DisplayText = vDisplayText;
        }
    }
}