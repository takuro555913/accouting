﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Account.Utils;
using static Account.Utils.AccountDefine;
using Account.Model;
using System.Drawing;

namespace Account.Grid
{
    public class AccountGrid : DataGridView
    {
        #region メンバ変数
        private List<AccountRow> FAccountRowList;
        #endregion メンバ変数

        public AccountGrid(UserEnum vUserEnum, List<AccountRow> vAccountRowList)
        {
            // TODO:共通部分の記述
            this.FAccountRowList = vAccountRowList;
            InitializeCommon();
            // TODO:モード別処理の記述
            switch (vUserEnum)
            {
                case UserEnum.Admin:
                    break;
                case UserEnum.GeneralUser:
                    InitializeGeneralUser();
                    break;
            }
        }

        #region 共通
        private void InitializeCommon()
        {
            // 借方、貸方カラムについて、ヘッダーの上からRectangleを描画するため、ヘッダー文字列を入れる必要はない
            this.Columns.Add(AccountHeader.Day.ToString(), AccountHeader.Day.GetDisplayText());
            this.Columns.Add(AccountHeader.Debit.ToString(), "");
            this.Columns.Add(AccountHeader.DebitMoney.ToString(), AccountHeader.DebitMoney.GetDisplayText());
            this.Columns.Add(AccountHeader.Credit.ToString(), "");
            this.Columns.Add(AccountHeader.CreditMoney.ToString(), AccountHeader.CreditMoney.GetDisplayText());
            this.Columns.Add(AccountHeader.Remarks.ToString(), AccountHeader.Remarks.GetDisplayText());

            for (int i = 0; i < this.ColumnCount; i++)
            {
                this.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            this.Paint += (_, __) =>
            {
                PaintHeader();
            };
            SetData();
        }

        /// <summary>
        /// ヘッダーを描画します
        /// </summary>
        private void PaintHeader()
        {
            using (var wGraphics = this.CreateGraphics())
            {
                var wDebitRectangle = this.GetCellDisplayRectangle((int)AccountHeader.Debit, -1, false);
                var wDebitMoneyRectangle = this.GetCellDisplayRectangle((int)AccountHeader.DebitMoney, -1, false);
                var wCreditRectangle = this.GetCellDisplayRectangle((int)AccountHeader.Credit, -1, false);
                var wCreditMoneyRectangle = this.GetCellDisplayRectangle((int)AccountHeader.CreditMoney, -1, false);

                wDebitRectangle.Width += wDebitMoneyRectangle.Width;
                wCreditRectangle.Width += wCreditMoneyRectangle.Width;
                wDebitRectangle.Y =
                wCreditRectangle.Y = 0;
                List<Rectangle> wRectangleList = new List<Rectangle>() { wDebitRectangle, wCreditRectangle };
                foreach (var wRectangle in wRectangleList)
                {
                    AccountHeader wAccountHeader = wRectangle.Equals(wDebitRectangle) ? AccountHeader.Debit : AccountHeader.Credit;
                    using (SolidBrush wBrush = new SolidBrush(this.DefaultCellStyle.BackColor))
                    {
                        wGraphics.FillRectangle(wBrush,
                                                int.Parse(wRectangle.Location.X.ToString()),
                                                wRectangle.Y,
                                                wRectangle.Width,
                                                wRectangle.Height);
                        using (Pen wPen = new Pen(this.DefaultCellStyle.ForeColor)) wGraphics.DrawRectangle(wPen, wRectangle);

                    }
                    TextRenderer.DrawText(wGraphics,
                                    wAccountHeader.GetDisplayText(),
                                    this.DefaultCellStyle.Font,
                                    wRectangle,
                                    this.DefaultCellStyle.ForeColor,
                                    TextFormatFlags.HorizontalCenter
                                    | TextFormatFlags.VerticalCenter);
                }
            }
        }
        #endregion 共通

        #region 一般ユーザ用
        #region Event
        #endregion Event

        #region Method
        /// <summary>
        /// 一般ユーザ用Initialize
        /// </summary>
        private void InitializeGeneralUser()
        {
            this.ReadOnly = true;
            this.AllowDrop = false;
            this.AllowUserToDeleteRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToOrderColumns = false;
            this.AllowUserToResizeRows = true;
            this.AllowUserToResizeColumns = true;
            this.BackgroundColor = System.Drawing.Color.LightYellow;
            this.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
            this.MultiSelect = false;
        }

        private void SetData() {
            foreach (var wAccountRow in FAccountRowList) {
                this.Rows.Add(wAccountRow.Day, 
                              wAccountRow.Debit.AccountTitle,
                              wAccountRow.Debit.Money,
                              wAccountRow.Credit.AccountTitle,
                              wAccountRow.Credit.Money,
                              wAccountRow.Remarks);
            }
        }
        #endregion Method
        #endregion 一般ユーザ用

        #region 管理者用

        #endregion 管理者用


    }
}
