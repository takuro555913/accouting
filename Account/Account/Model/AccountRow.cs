﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Model
{
    [System.Runtime.Serialization.DataContract]
   public class AccountRow
    {
        [System.Runtime.Serialization.DataMember()]
        public AccountCell Debit = new AccountCell();

        [System.Runtime.Serialization.DataMember()]
        public AccountCell Credit = new AccountCell();

        [System.Runtime.Serialization.DataMember()]
        public string Day = DateTime.Today.ToString("dd");

        [System.Runtime.Serialization.DataMember()]
        public string Remarks;

        [System.Runtime.Serialization.DataMember()]
        public int ParentIndex = -1;

        [System.Runtime.Serialization.DataMember()]
        public int MyIndex;

        public AccountRow(int vIndex) {
            this.MyIndex = vIndex;
        }

        /// <summary>
        /// Debit,Credit共に空か判定します
        /// </summary>
        /// <returns>
        /// 両方空の場合：true
        /// </returns>
        public bool GetIsEmpty() {
            if (this.Debit.Money == 0 && this.Credit.Money == 0) return true;
            return false;
        }

        /// <summary>
        /// グリッドの行データをセットするコンストラクタ
        /// </summary>
        /// <param name="vMyIndex">インデックス</param>
        /// <param name="vParentIndex">親インデックス</param>
        /// <param name="vDay">日</param>
        /// <param name="vDebitTitle">借方</param>
        /// <param name="vDebitMoney">借方金額</param>
        /// <param name="vCreditTitle">貸方</param>
        /// <param name="vCreditMoney">貸方金額</param>
        /// <param name="vRemarks">備考</param>
        public AccountRow(int vMyIndex, int vParentIndex, string vDay, string vDebitTitle, int vDebitMoney, string vCreditTitle, int vCreditMoney, string vRemarks)
        {
            this.MyIndex = vMyIndex;
            this.ParentIndex = vParentIndex;
            this.Day = vDay;
            this.Debit.AccountTitle = vDebitTitle;
            this.Debit.Money = vDebitMoney;
            this.Credit.AccountTitle = vCreditTitle;
            this.Credit.Money = vCreditMoney;
            this.Remarks = vRemarks;
        }

        /// <summary>
        /// Debit,Creditの金額が同じか判定します
        /// </summary>
        /// <returns>
        /// 同じ場合：true
        /// </returns>
        public bool GetIsEqualTotal() {
            if (this.Debit.Money == this.Credit.Money) return true;
            return false;
        }
    }
}
