﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Record
{
    class RecordDocument {
        private List<Records> FRecordDoc;

    }
    class Records
    {
        public List<BSRecord> FBSRecords;
        public List<ShihonRecord> FShihonRecords;
        private string FYear;
        private string FMonth;

        public string GetYear
        {
            get { return FYear; }
        }

        public string GetMonth
        {
            get { return FMonth; }
        }

        public Records()
        {
            this.FYear = DateTime.Now.ToString("yyyy");
            this.FMonth = DateTime.Now.Month.ToString();
            this.FBSRecords = new List<BSRecord>();
            this.FShihonRecords = new List<ShihonRecord>();
        }

        public void AddRecord()
        {
            BSRecord wBSRecord = new BSRecord(new Tuple<string, int?>("テスト1", 1), new Tuple<string, int?>("テスト2", 2));
            FBSRecords.Add(wBSRecord);
        }
    }
}
