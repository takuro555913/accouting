﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Account.Model;

namespace Account.Utils
{
    public static class TestData
    {
        /// <summary>
        /// グリッド表示用テストデータ
        /// </summary>
        /// <returns></returns>
        public static List<AccountRow> GetTestAccountRowList()
        {
            List<AccountRow> wResult = new List<AccountRow>();
            wResult.Add(new AccountRow(1) {
                Remarks = "１番目"
            });
            wResult.Add(new AccountRow(2)
            {
                Remarks = "２番目",
                ParentIndex = 2
            });
            wResult.Add(new AccountRow(3)
            {
                Remarks = "３番目",
                ParentIndex = 2
            });
            wResult.Add(new AccountRow(4)
            {
                Remarks = "４番目"
            });
            wResult.Add(new AccountRow(5)
            {
                Remarks = "５番目"
            });
            wResult.Add(new AccountRow(6)
            {
                Remarks = "６番目"
            });
            wResult.Add(new AccountRow(7)
            {
                Remarks = "７番目"
            });
            wResult.Add(new AccountRow(8)
            {
                Remarks = "８番目"
            });
            wResult.Add(new AccountRow(9)
            {
                Remarks = "９番目"
            });
            return wResult;

        }
        /// <summary>
        /// メール送信テスト
        /// </summary>
        public static void SendMailTest() {
  
        }

        private static void Click_DisplayButton()
        {

        }

    }
}
